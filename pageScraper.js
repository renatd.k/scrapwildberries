const scraperObject = {
    url: 'https://www.wildberries.ru/catalog/obuv/muzhskaya',
    async scraper(browser){
        let page = await browser.newPage();
        console.log(`Navigating to ${this.url}...`);
        // Navigate to the selected page
        await page.goto(this.url);
        // Wait for the required DOM to be rendered
        await page.waitForSelector('.catalog_main_table');
        // Get the link to all products 
        let urls = await page.$$eval('.dtList', links => {
            // Extract the links from the data
            links = links.map(el => el.querySelector('.dtList-inner > a').href)
            return links;
        });
        
        console.log("urls", urls);
        
        let scrapedData = {}; 
        scrapedData.categories = await page.$$eval('.sidemenu ul > li', items => {
            items = items.map(el => { 
              const a = el.querySelector('a');
              return {name: a.textContent, src: a.href};
            });
            return items;
        });
        
        console.log("Categories", scrapedData.categories);
        
        let i = 1;
        // Loop through each of those links, open a new page instance and get the relevant data from them
        let pagePromise = (link) => new Promise(async(resolve, reject) => {
            let dataObj = {};
            let newPage = await browser.newPage();
            await newPage.goto(link);
            dataObj['position'] = i++;
            dataObj['brand'] = await newPage.$eval('.brand-and-name > .brand', text => text.textContent);
            dataObj['name'] = await newPage.$eval('.brand-and-name > .name', text => text.textContent);
            dataObj['article'] = await newPage.$eval('.article > span', text => text.textContent);
            dataObj['description'] = await newPage.$eval('.description-text > p', text => text.textContent);
            resolve(dataObj);
            await newPage.close();
        });
        
        scrapedData.products = []; 
        for (link in urls) {
            let currentPageData = await pagePromise(urls[link]);
            console.log(currentPageData);
            scrapedData.products.push(currentPageData);
        }

        return scrapedData;
    }
}

module.exports = scraperObject;
